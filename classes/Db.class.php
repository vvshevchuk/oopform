<?php


class Db {
    private $servername;
    private $uname;
    private $pass;
    private $dbname;
    private $charset;

    public function connect(){
        $this->servername = "localhost";
        $this->uname = "vitalik";
        $this->pass = "1";
        $this->dbname = "my_form";
        $this->charset = "utf8mb4";

        try{
            $dsn = "mysql:host={$this->servername};dbname={$this->dbname};charset={$this->charset}";
            $pdo = new PDO($dsn, $this->uname, $this->pass);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            //echo "Connected successfully";
            return $pdo;
        } catch (PDOException $e){
            echo "Connection PDO DB is failed: " . $e->getMessage();
        }
    }

}