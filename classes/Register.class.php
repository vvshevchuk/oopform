<?php
include "Db.class.php";
class Register extends Db {

    // define variables and set to empty values
    public $data = "";
    public $files = "";
    public $username = "";
    public $password = "";
    public $password_confirm = "";
    public $email = "";
    public $image = "default_user.png";
    public $message = "";

    //method recive $_POST
    public function __construct($data,$files){
        $this->data = $data;
        $this->files = $files;
        $this->username = $this->test_input($data["username"]);
        $this->email = $this->test_input($data["email"]);
        $this->password = md5($this->test_input($data["password"]));
        $this->password_confirm = md5($this->test_input($data["password_confirm"]));
    }

    //safe data
    public function test_input($data){
        //Strip whitespace (or other characters) from the beginning and end of a string
        $data = trim($data);
        //Un-quotes a quoted string /'
        $data = stripslashes($data);
        //Convert special characters to HTML entities
        $data = htmlspecialchars($data);
        return $data;
    }

    public function validateRegisterForm(){
        if (!$this->validateUsername()) return false;
        elseif(!$this->validatePassword()) return false;
        elseif(!$this->validateEmail()) return false;
        elseif ($this->password !== $this->password_confirm) {
            $this->message = 'Password and Confirm password should match!';
            return false;
        } else return true;
    }

    public function validateUsername(){
        $data = $this->data;
        if (empty($data["username"])){
            $this->message = "Username is required";
            return false;
            // check if name only contains letters and whitespace
        } elseif (!preg_match("/^[a-zA-Z-' ]*$/", $data["username"])) {
            $this->message = "Only letters and white space allowed to username";
            return false;
        }else return true;
    }

    public function validatePassword(){
        $data = $this->data;
        if (empty($data["password"])) {
            $this->message = "Password is required";
            return false;
        }elseif (!preg_match("/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{3,16}$/", $data['password'])) {
            $this->message = "Password must include at least one upper case letter, 
                            one lower case letter, and one numeric digit";
            return false;
        }else return true;
    }

    public function validateEmail(){
        $data = $this->data;
        if (empty($data["email"])) {
            $this->message = "Email is required";
            return false;
        }// check if e-mail address is well-formed
        elseif (!filter_var($data["email"], FILTER_VALIDATE_EMAIL)) {
            $this->message = "Invalid email format";
            return false;
        }else return true;
    }

    public function checkUserExist() {
        if (!$this->checkUsername()) return false;
        elseif (!$this->checkEmail()) return false;
        else return true;
    }

    public function checkUser() {
        $sql1 = "SELECT * FROM users WHERE username = '$this->username' AND password = '$this->password'";
        $stmt = $this->connect()->prepare($sql1);
        $stmt->execute();
        $res = $stmt->fetch(PDO::FETCH_BOTH);
        if (!$res) {
            $this->message = "Uncorrect username or password";
            return false;
        }else return $res;
    }

    public function checkUsername() {
        $data = $this->data;
        $u = $data['username'];
        $sql1 = "SELECT * FROM users WHERE username='$u'";
        $stmt = $this->connect()->prepare($sql1);
        $stmt->execute();
        $res = $stmt->fetch(PDO::FETCH_BOTH);
        if ($res) {
            $this->message = "You can not use {$this->username}  username. Please input another username";
            return false;
        }else return true;
    }

    public function checkEmail() {
        $sql2 = "SELECT * FROM users WHERE email='$this->email'";
        $stmt = $this->connect()->prepare($sql2);
        $stmt->execute();
        $res = $stmt->fetch(PDO::FETCH_BOTH);
        if ($res) {
            $this->message = "You can not use email {$this->email}. Please input another email";
            return false;
        }else return true;
    }

    public function uploadImage(){
        $files = $this->files;
        if (!empty($files['image'])) {
            $file_name = $files['image']['name'];
            $file_size = $files['image']['size'];
            $file_tmp = $files['image']['tmp_name'];
            $file_type = $files['image']['type'];
            $file_dir = "images/{$file_name}";

            if (move_uploaded_file($file_tmp, $file_dir)) {
                echo "image success upload";
                $this->image = $file_name;
            } else {
                echo "image NO success upload";
            }
        } else {
            echo 'Empty files array';
        }

    }

    public function setUserRegister() {
        $this->uploadImage();
        $sql3 = "INSERT INTO users (username, email, password, image)
                VALUES (:username, :email, :password, :image)";
        $stmt = $this->connect()->prepare($sql3);
        $stmt->bindParam(':username', $this->username);
        $stmt->bindParam(':email', $this->email);
        $stmt->bindParam(':password', $this->password);
        $stmt->bindParam(':image', $this->image);
        $stmt->execute();
        return true;
    }
}