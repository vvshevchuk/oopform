<?php
include 'Login.class.php';
class UserUpdate extends Login {
    // define variables and set to empty values
    public $data = "";
    public $files = "";
    public $username = "";
    public $password = "";
    public $newpass = "";
    public $newpassconf = "";
    public $email = "";
    public $image = "";
    public $message = "";
    public $ses_id = "";
    public $ses_data = "";

    //method recive $_POST, $_FILES
    public function __construct($data,$files){
        $this->data = $data;
        $this->files = $files;
        $this->username = $this->test_input($data["username"]);
        $this->email = $this->test_input($data["email"]);
        $this->password = md5($this->test_input($data["password"]));
        $this->newpass = md5($this->test_input($data["newpass"]));
        $this->newpassconf = md5($this->test_input($data["newpassconf"]));
        $this->image = $this->test_input($files['image']['name']);
    }

    public function validateUpdateForm(){
        $this->getUserSession();
        $ses_data = $this->ses_data;
        if(!$this->validateUsername()) return false;
        elseif(!$this->validateEmail()) return false;
        elseif (!$this->validatePasswords()) return false;
        elseif (($this->username == $ses_data['username']) and ($this->email== $ses_data['email']) and
                empty($this->data['newpass']) and empty($this->files['image']['name'])){
            $this->message = "Have no any different data to save";
            return false;
        }
        else return true;
    }



    public function validatePasswords(){
        $ses_data = $this->ses_data;
        if(!($this->newpass == $this->newpassconf)){
            $this->message = "New password and new password confirm should match";
            return false;
        }elseif(!empty($this->password) and empty($this->newpass)){
            $this->message = "New password and new password confirm should be filled";
            return false;
        }elseif(empty($this->password) and !empty($this->newpass)){
            $this->message = "Please, input old password to confirm a new";
            return false;
        }elseif(!empty($this->data['password']) and !empty($this->data['newpass']) and ($this->password !== $ses_data['password'])){
            $this->message = "Old password is invalid";
            return false;
        } elseif(!empty($this->data['password']) and !empty($this->data['newpass']) and ($this->password == $this->newpass)){
            $this->message = "New password and old must be different";
            return false;
        }elseif(!empty($this->data['password']) and !preg_match("/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{3,16}$/", $this->data['password'])) {
            $this->message = 'Password must include at least one upper case letter, 
                    one lower case letter, and one numeric digit';
            return false;
        }elseif (!empty($this->data['newpass']) and !preg_match("/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{3,16}$/", $this->data['newpass'])) {
            $this->message = "New password must include at least one upper case letter, 
                    one lower case letter, and one numeric digit";
            return false;
        }elseif (!empty($this->data['newpassconf']) and !preg_match("/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{3,16}$/", $this->data['newpassconf'])) {
            $this->message = "New password confirm must include at least one upper case letter, 
                    one lower case letter, and one numeric digit";
            return false;
        }else return true;
    }

    public function updateUsername() {
        $ses_data = $this->ses_data;
        if ($this->username !== $ses_data['usename']) {
            $sql_upd_un = "UPDATE users SET username = :username WHERE id = :userid";
            $stmt = $this->connect()->prepare($sql_upd_un);
            $stmt->bindParam(':username', $this->username);
            $stmt->bindParam(':userid', $ses_data['userid']);
            $stmt->execute();
            return true;
        }else return false;
    }

    public function updateUEmail() {
        $ses_data = $this->ses_data;
        if ($this->email !== $ses_data['email']) {
            $sql_upd_em = "UPDATE users SET email = :email WHERE id = :userid";
            $stmt = $this->connect()->prepare($sql_upd_em);
            $stmt->bindParam(':email', $this->email);
            $stmt->bindParam(':userid', $ses_data['userid']);
            $stmt->execute();
            return true;
        }else return false;
    }

    public function updatePassword() {
        $ses_data = $this->ses_data;
        if (!empty($this->data['newpass']) and ($this->password = $ses_data['password'])) {
            $sql_upd_pa = "UPDATE users SET password = :password WHERE id = :userid";
            $stmt = $this->connect()->prepare($sql_upd_pa);
            $stmt->bindParam(':password', $this->newpass);
            $stmt->bindParam(':userid', $ses_data['userid']);
            $stmt->execute();
            return true;
        }else return false;
    }
    public function updateImage() {
        $ses_data = $this->ses_data;
        if (!empty($this->files['image']['name'])) {
            $this->uploadImage();
            $sql_upd_img = "UPDATE users SET image = :image WHERE id = :userid";
            $stmt = $this->connect()->prepare($sql_upd_img);
            $stmt->bindParam(':image', $this->image);
            $stmt->bindParam(':userid', $ses_data['userid']);
            $stmt->execute();
            return true;
        }else return false;
    }
    //after successful all parts - logout and redirect to logon
   // header('Location:login.php?id=update');

}
