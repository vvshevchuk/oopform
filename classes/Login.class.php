<?php
// $data = $_POST;
include 'Register.class.php';
class Login extends Register {

    // define variables and set to empty values
    public $data = "";
    public $username = "";
    public $password = "";
    public $message = "";
    public $ses_id = "";
    public $ses_data = "";

    //method recive $_POST
    public function __construct($data){
       $this->data = $data;
       $this->username = $this->test_input($data["username"]);
       $this->password = md5($this->test_input($data["password"]));
    }

    public function validateLoginForm(){
        if(!$this->validateUsername()) return false;
        elseif(!$this->validatePassword()) return false;
        else return true;
    }

    public function checkUser() {
    $sql1 = "SELECT * FROM users WHERE username = '$this->username' AND password = '$this->password'";
    $stmt = $this->connect()->prepare($sql1);
    $stmt->execute();
    $res = $stmt->fetch(PDO::FETCH_BOTH);
    if (!$res) {
        $this->message = "Uncorrect username or password";
        return false;
    }else return $res;
    }

    public function setUserSession() {
    session_start();
    $_SESSION['check'] = true;
    $ses_id = session_id();
    $ses = $this->checkUser();
    $ses_data1 = ["username" => $ses['username'], "email" => $ses['email'],
        "password" => $ses['password'], "userid" => $ses['id'], "image" => $ses['image']];
    $ses_data = base64_encode(serialize($ses_data1));
    $sql_ses = "INSERT INTO sessions (ses_id, ses_data)
                VALUES (:ses_id, :ses_data)";
    $stmt = $this->connect()->prepare($sql_ses);
    $stmt->bindParam(':ses_id', $ses_id);
    $stmt->bindParam(':ses_data', $ses_data);
    $stmt->execute();
    return true;
    /*
    header('Location:index.php');
    */
    }

    public function getUserSession() {
        session_start();
        $this->ses_id = session_id();
        $sesinfo = "SELECT * FROM sessions WHERE ses_id='$this->ses_id'";
        $stmt = $this->connect()->prepare($sesinfo);
        $stmt->execute();
        $res = $stmt->fetch(PDO::FETCH_BOTH);
        if (!empty($res)) {
                $ses_data1 = $res["ses_data"];
                $this->ses_data = unserialize(base64_decode($ses_data1));
            }else echo "session_data is failed";
        // result: $ses_data['username'], $ses_data['email'] and $ses_data['password']
    }

    public function userLogout() {
        $ses_db_del = "DELETE FROM sessions WHERE ses_id='$this->ses_id'";
        $stmt = $this->connect()->prepare($ses_db_del);
        $stmt->execute();
        session_destroy();
        setcookie(session_name(), session_id(), time()-3600);
    }



}
?>
