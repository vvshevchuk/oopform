<?php
require 'classes/UserUpdate.class.php';
$userupdate = new UserUpdate($_POST,$_FILES);
$userupdate->getUserSession();
$ses_data = $userupdate->ses_data;
$message='';
if ($_SERVER["REQUEST_METHOD"] == "POST"){

    if (!$userupdate->validateUpdateForm()){
        $message = $userupdate->message;
    }else{
        $userupdate->updateUsername();
        $userupdate->updateUEmail();
        $userupdate->updatePassword();
        $userupdate->updateImage();
        $userupdate->userLogout();
        header('Location:login.php?id=update');
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>My profile</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
    <link rel="stylesheet" type="text/css" href="profile.css">

</head>
<body>
<div class="wrapper">
    <div class="container">
        <div class="login-form">
            <h1>Here you can edit your profile</h1>
            <p><?=$message;?></p>
            <form action="<?= htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST" enctype="multipart/form-data">
                <input type="text" placeholder="Username" class="input" name="username" value="<?=$ses_data['username'];?>" required><br />
                <input type="password" placeholder="Old password" class="input" name="password" value="<?=$_POST['password'];?>"><br/>
                <input type="password" placeholder="New password" class="input" name="newpass" value="<?=$_POST['newpass'];?>"><br/>
                <input type="password" placeholder="New password confirm" class="input" name="newpassconf" value="<?=$_POST['newpassconf'];?>"><br/>
                <input type="email" placeholder="Your Email Address" class="input" name='email' value="<?=$ses_data['email'];?>" required><br />
                <input type="file" name="image"/>
                <input type="submit" value="Save"  class="btn"/>
                <a href='index.php' class='cancel'>Cancel</a>
            </form>
        </div>

    </div>
</div>

</body>
</html>