<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <title>Main page</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
    <link rel="stylesheet" type="text/css" href="indexstyle.css">
</head>
<body>
<div class="content">
<?php if(isset($_SESSION['check'])){
    require_once 'classes/Login.class.php';
    $userlogin = new Login($_POST);
    $userlogin->getUserSession();
    $ses_data = $userlogin->ses_data;
    echo "<img src='images/{$ses_data['image']}' >";
    echo "Hi, {$ses_data['username']}. Your email: {$ses_data['email']}.</br></br>";
    echo "<a href='index.php?id=logout'class='logout'>Log out</a>";
    echo "<a href='profile.php'class='upd'>My profile</a>";
}else {
    echo 'Hello, gest! Plese log in or sign up.</br></br>';
    echo "<a href='login.php' class='login'>Log in</a>";
    echo "<a href='register.php' class='signup'>Sign up</a>";
}
//destroy session from log ut
if ($_GET['id'] == 'logout'){
    $userlogin->userLogout();
    header('Location:index.php');
}
?>
</div>
</body>
</html>
