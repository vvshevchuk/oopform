<?php
$message='';
if ($_SERVER["REQUEST_METHOD"] == "POST"){
    require 'classes/Register.class.php';
    $userreg = new Register($_POST,$_FILES);
    if (!$userreg->validateRegisterForm()){
        $message = $userreg->message;
    }elseif(!$userreg->checkUserExist()){
        $message = $userreg->message;
    }else {
        $userreg->setUserRegister();
        header('Location: login.php?id=reg');
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Sign up</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
    <link rel="stylesheet" type="text/css" href="mystile2.css">
</head>
<body>
<div class="wrapper">
    <div class="container">

        <a href="register.php"><div class="signup" id="regsignup">Sign Up</div></a>
        <a href="login.php"><div class="login">Log In</div></a>

        <div class="signup-form">
            <?=$message;?>
            <form action="<?= htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST" enctype="multipart/form-data">
            <input type="text" placeholder="Choose a Username" class="input" name='username' value="<?=$_POST['username'];?>" required><br />
            <input type="password" placeholder="Choose a Password" class="input" name='password' value="<?=$_POST['password'];?>" required ><br />
            <input type="password" placeholder="Confirm a Password" class="input" name='password_confirm' value="<?=$_POST['password_confirm'];?>" required><br />
            <input type="email" placeholder="Your Email Address" class="input" name='email' value="<?=$_POST['email'];?>" required><br />
            <label>Your avatar: </label>
            <input type="file" name="image"/>
            <input type="submit" value="Get Started"  class="btn"/>
            </form>
        </div>
    </div>
</div>

</body>
</html>