<?php
$message='';
if ($_SERVER["REQUEST_METHOD"] == "POST"){
    require 'classes/Login.class.php';
    $userlogin = new Login($_POST);
    if (!$userlogin->validateLoginForm()){
        $message = $userlogin->message;
    }elseif(!$userlogin->checkUser()){
        $message = $userlogin->message;
    }else {
        $userlogin->setUserSession();
        header('Location:index.php');
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Log in</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
    <link rel="stylesheet" type="text/css" href="mystile2.css">

</head>
<body>
<div class="wrapper">
    <div class="container">
        <a href="register.php"><div class="signup">Sign Up</div></a>
        <a href="login.php"><div class="login" id="loglogin">Log In</div></a>
        <div class="login-form">
            <?php if ($_GET['id']== 'reg'){
                $message = "Your registration was successful.</br>
                        Please, log in site";}?>
            <?php if ($_GET['id']== 'update'){
                $message = "Your updating was successful.</br>
                        Please, log in site to view changes";}?>
            <?php

            ?>
            <?=$message;?>
            <form action="<?= htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
                <input type="text" placeholder="Username" class="input" name="username" value="<?=$_POST['username'];?>" required><br />
                <input type="password" placeholder="Password" class="input" name="password" value="<?=$_POST['password'];?>" required><br/>
                <input type="submit" value="log in"  class="btn"/>
            </form>
        </div>

    </div>
</div>

</body>
</html>